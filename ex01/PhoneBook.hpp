#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <vector>
#include <iostream>
#include <unistd.h>
#include "Contact.hpp"

#define MAX_CONTACT	2

class PhoneBook
{
private:

	std::vector<Contact>	_contacts;

public:

	PhoneBook();
	~PhoneBook();
	void	add(Contact &contact);
	size_t	display_contacts(void);
	void	retrieve_contact(int idx);
};

#endif
