#include "PhoneBook.hpp"
#include "Contact.hpp"
#include <cstdlib>
#include <cstring>

static void	cmd_add(PhoneBook &book)
{
	std::string	fname, lname, nname, phone, secret;

	std::cout	<< "..Enter new contact's first name: ";
	std::getline(std::cin, fname);

	std::cout	<< "..Enter " << fname << "'s last name: ";
	std::getline(std::cin, lname);
	
	std::cout	<< "..Enter " << fname << " " << lname << "'s nick name: ";
	std::getline(std::cin, nname);
	
	std::cout	<< "..Now enter " << nname << "'s phone number: ";
	std::getline(std::cin, phone);
	
	std::cout	<< "..Finally what is this person's darkest secret: ";
	std::getline(std::cin, secret);

	if (fname.empty() || lname.empty() || nname.empty() || phone.empty() || secret.empty())
	{
		std::cout << "All fields must be filled with informations. No contact was added\n";
		return ;
	}

	Contact	new_contact(fname, lname, nname, phone, secret);
	book.add(new_contact);
}

static void	cmd_search(PhoneBook &book)
{
	size_t	n_contacts = book.display_contacts();

	if (!n_contacts)
		return ;

	std::string	input;
	std::cout << "..OK now what? ";
	std::getline(std::cin, input);

	if (input.empty())
		std::cout << "No input detected. Assuming 0\n";

	// Check user's input
	for (std::string::iterator c = input.begin(); c != input.end(); c++)
		if (!isdigit(*c))
		{
			book.retrieve_contact(-1);
			return ;
		}
	book.retrieve_contact(atoi(input.c_str()));
}

int	main()
{
	std::cout		<< " _____                        _____ _               _____         _   \n"
					<< "|     |___ ___ ___ ___ _ _   |  _  | |_ ___ ___ ___| __  |___ ___| |_ \n"
					<< "|   --|  _| .'| . | . | | |  |   __|   | . |   | -_| __ -| . | . | '_|\n"
					<< "|_____|_| |__,|  _|  _|_  |  |__|  |_|_|___|_|_|___|_____|___|___|_,_|\n"
					<< "              |_| |_| |___|                                           \n";
	PhoneBook	book;
	
	std::string	cmd;
	while (true)
	{
		std::cout	<< "\n-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.\n"
					<< "What do you want? ";
		std::getline(std::cin, cmd);
		if (cmd == "EXIT")
			return (0);
		else if (cmd == "ADD")
			cmd_add(book);
		else if (cmd == "SEARCH")
			cmd_search(book);
	}
}
