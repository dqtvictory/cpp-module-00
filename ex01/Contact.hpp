#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <iostream>

class Contact
{
private:

	std::string	_fname;
	std::string	_lname;
	std::string	_nname;
	std::string	_phone;
	std::string	_secret;

public:

	Contact(
		std::string fname,
		std::string lname,
		std::string nname,
		std::string phone,
		std::string secret
	);
	~Contact();

	std::string	get_fname(void);
	std::string	get_lname(void);
	std::string	get_nname(void);
	std::string	get_phone(void);
	std::string	get_secret(void);
	void		display_info(void);
};

#endif
