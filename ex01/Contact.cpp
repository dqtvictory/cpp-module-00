#include "Contact.hpp"

Contact::Contact(
	std::string fname,
	std::string lname,
	std::string nname,
	std::string phone,
	std::string secret
) :	_fname(fname),
	_lname(lname),
	_nname(nname),
	_phone(phone),
	_secret(secret)
{}

Contact::~Contact(){}

std::string	Contact::get_fname(void)
{
	return (_fname);
}

std::string	Contact::get_lname(void)
{
	return (_lname);
}

std::string	Contact::get_nname(void)
{
	return (_nname);
}

std::string	Contact::get_phone(void)
{
	return (_phone);
}

std::string	Contact::get_secret(void)
{
	return (_secret);
}

void	Contact::display_info(void)
{
	std::cout	<< "- First name:     " << _fname << "\n"
				<< "- Last name:      " << _lname << "\n"
				<< "- Nick name:      " << _nname << "\n"
				<< "- Phone number:   " << _phone << "\n"
				<< "- Darkest secret: " << _secret << "\n";
}
