#include "PhoneBook.hpp"
#include "Contact.hpp"

// Truncate str to len characters if longer than len or else left pad with spaces
static std::string	truncate_leftpad(std::string str, size_t len)
{
	if (str.size() >= len)
		return (str.substr(0, len - 1) + '.');
	
	std::string	res(str);
	for (size_t i = 0; i < len - str.size(); i++)
		res = ' ' + res;
	return (res);
}

PhoneBook::PhoneBook() {}

PhoneBook::~PhoneBook() {}

void	PhoneBook::add(Contact &new_contact)
{
	size_t	count = _contacts.size();
	
	// Delete first contact if book is full
	if (count == MAX_CONTACT)
		_contacts.erase(_contacts.begin());
	_contacts.push_back(new_contact);
	
	std::cout	<< "Welcome " << new_contact.get_fname()
				<< " " << new_contact.get_lname()
				<< " (" << new_contact.get_nname() << ") to the Book!\n";
}

size_t	PhoneBook::display_contacts(void)
{
	size_t	count = _contacts.size();
	Contact	*contact;

	std::cout		<< "┌──────────┬──────────┬──────────┬──────────┐\n"
					<< "│     index│first name│ last name│  nickname│\n"
					<< "├──────────┼──────────┼──────────┼──────────┤\n";

	if (!count)
		std::cout	<< "│          LOL NOTHING TO SEE HERE          │\n";

	for (size_t	i = 0; i < count; i++)
	{
		contact = &(_contacts[i]);
		std::cout	<< "│         " << i
					<< "│" << truncate_leftpad(contact->get_fname(), 10)
					<< "│" << truncate_leftpad(contact->get_lname(), 10)
					<< "│" << truncate_leftpad(contact->get_nname(), 10)
					<< "│\n";
	}
	std::cout 		<< "└──────────┴──────────┴──────────┴──────────┘\n";

	return (count);
}

void	PhoneBook::retrieve_contact(int idx)
{
	size_t	count = _contacts.size();
	Contact	*contact;

	if (idx < 0 || idx >= (int)count)
	{
		std::cout << "Segmentation fault\n";
		sleep(2);
		std::cout << "... nah, just kidding\n";
		return ;
	}
	contact = &(_contacts[idx]);
	contact->display_info();
}
