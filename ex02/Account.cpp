#include "Account.hpp"
#include <ctime>
#include <iostream>
#include <sstream>

int	Account::_nbAccounts = 0;
int	Account::_totalAmount = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

static std::string	format_num(int num)
{
	std::string			res;
	std::stringstream	ss;

	ss << num;
	ss >> res;

	if (num < 10)
		res = '0' + res;
	return (res);
}

int	Account::getNbAccounts(void)
{
	return (_nbAccounts);
}

int	Account::getTotalAmount(void)
{
	return (_totalAmount);
}

int	Account::getNbDeposits(void)
{
	return (_totalNbDeposits);
}

int	Account::getNbWithdrawals(void)
{
	return (_totalNbWithdrawals);
}

void	Account::displayAccountsInfos(void)
{
	_displayTimestamp();
	std::cout	<< "accounts:" << _nbAccounts
				<< ";total:" << _totalAmount
				<< ";deposits:" << _totalNbDeposits
				<< ";withdrawals:" << _totalNbWithdrawals
				<< '\n';
}

Account::Account(int initial_deposit) : _accountIndex(_nbAccounts),
										_amount(initial_deposit),
										_nbDeposits(0),
										_nbWithdrawals(0)
{
	_displayTimestamp();
	std::cout	<< "index:" << _accountIndex
				<< ";amount:" << _amount
				<< ";created\n";
	_nbAccounts++;
	_totalAmount += initial_deposit;
}

Account::~Account()
{
	_displayTimestamp();
	std::cout	<< "index:" << _accountIndex
				<< ";amount:" << _amount
				<< ";closed\n";
	_nbAccounts--;
	_totalAmount -= _amount;
}

void	Account::makeDeposit(int deposit)
{
	_nbDeposits++;
	_totalNbDeposits++;
	_displayTimestamp();
	std::cout	<< "index:" << _accountIndex
				<< ";p_amount:" << _amount
				<< ";deposit:" << deposit
				<< ";amount:" << (_amount + deposit)
				<< ";nb_deposits:" << _nbDeposits
				<< '\n';
	_amount += deposit;
	_totalAmount += deposit;
}

bool	Account::makeWithdrawal(int withdrawal)
{
	_displayTimestamp();
	std::cout	<< "index:" << _accountIndex
				<< ";p_amount:" << _amount
				<< ";withdrawal:";
	
	if (withdrawal > _amount)
	{
		std::cout << "refused\n";
		return (false);
	}
	_nbWithdrawals++;
	_totalNbWithdrawals++;
	std::cout	<< withdrawal
				<< ";amount:" << (_amount - withdrawal)
				<< ";nb_withdrawals:" << _nbWithdrawals
				<< '\n';
	_amount -= withdrawal;
	_totalAmount -= withdrawal;
	return (true);
}

int	Account::checkAmount(void) const
{
	return (_amount);
}

void	Account::displayStatus(void) const
{
	_displayTimestamp();
	std::cout	<< "index:" << _accountIndex
				<< ";amount:" << _amount
				<< ";deposits:" << _nbDeposits
				<< ";withdrawals:" << _nbWithdrawals
				<< '\n';
}

void	Account::_displayTimestamp( void )
{
	std::time_t	t		= std::time(NULL);
	std::tm		*now	= std::localtime(&t);

	std::string	year 	= format_num(now->tm_year + 1900);
	std::string	month 	= format_num(now->tm_mon + 1);
	std::string	day 	= format_num(now->tm_mday);
	std::string	hour 	= format_num(now->tm_hour);
	std::string	minute	= format_num(now->tm_min);
	std::string	second	= format_num(now->tm_sec);

	std::cout << '[' + year + month + day + '_' + hour + minute + second + "] ";
}
